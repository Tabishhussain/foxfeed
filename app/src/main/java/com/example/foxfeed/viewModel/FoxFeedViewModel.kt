package com.example.foxfeed.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.foxfeed.model.data.FeedItem
import com.example.foxfeed.model.repositories.FeedRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class FoxFeedViewModel @Inject constructor(
    private val feedRepository: FeedRepository
) : ViewModel(), FeedRepository.FeedLoadingListener {

    // LiveData object for feeds to observer and reflect changes on UI
    val feedLiveData = MutableLiveData<List<FeedItem>>()
    // LiveData Object for errors to Log them and in future show dialog.
    val error = MutableLiveData<String?>()

    fun loadFeeds() = viewModelScope.launch {
        feedRepository.getFeed(this@FoxFeedViewModel)
    }

    override fun onFinishLoading(feeds: List<FeedItem>) {
        feedLiveData.postValue(feeds)
    }

    override fun onFailure(message: String?) {
        error.postValue(message)
    }
}