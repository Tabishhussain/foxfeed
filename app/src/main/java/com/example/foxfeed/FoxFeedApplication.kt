package com.example.foxfeed

import android.app.Application
import com.example.foxfeed.dagger.AppComponent
import com.example.foxfeed.dagger.DaggerAppComponent

class FoxFeedApplication: Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.create()
    }
}