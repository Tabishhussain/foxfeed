package com.example.foxfeed.dagger.modules

import com.example.foxfeed.model.service.MockFeedClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object FoxFeedModule {

    @Singleton
    @Provides
    fun provideMockFeedClient() = MockFeedClient()
}