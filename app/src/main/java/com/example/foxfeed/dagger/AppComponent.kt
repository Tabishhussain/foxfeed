package com.example.foxfeed.dagger

import com.example.foxfeed.dagger.modules.FoxFeedModule
import com.example.foxfeed.view.activities.FoxFeedActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        FoxFeedModule::class
    ]
)
interface AppComponent {

    fun inject(activity: FoxFeedActivity)
}