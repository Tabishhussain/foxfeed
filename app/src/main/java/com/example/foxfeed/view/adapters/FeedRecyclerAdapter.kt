package com.example.foxfeed.view.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.foxfeed.databinding.*
import com.example.foxfeed.model.data.FeedItem

class FeedRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var feedList: List<FeedItem> = emptyList()

    override fun getItemViewType(position: Int): Int {
        return feedList[position].component.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> BigTopFeedViewHolder(
                ItemFeedListBigTopBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            1 -> RiverFeedViewHolder(
                ItemFeedListRiverBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            2 -> AdFeedViewHolder(
                ItemFeedListAdBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            3 -> HouseAdFeedViewHolder(
                ItemFeedListHouseAdBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> SlideShowFeedViewHolder(
                ItemFeedListSlideshowBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feedItem = feedList[position]
        when (feedItem.component.ordinal) {
            0 -> (holder as BigTopFeedViewHolder).binding.headline.text = feedItem.headline
            1 -> (holder as RiverFeedViewHolder).binding.headline.text = feedItem.headline
            2 -> (holder as AdFeedViewHolder).binding.headline.text = feedItem.headline
            3 -> (holder as HouseAdFeedViewHolder).binding.headline.text = feedItem.headline
            else -> (holder as SlideShowFeedViewHolder).binding.headline.text = feedItem.headline
        }
    }

    override fun getItemCount(): Int {
        return feedList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setFeedList(listOfFeed: List<FeedItem>) {
        feedList = listOfFeed
        notifyDataSetChanged()
    }

    class AdFeedViewHolder(val binding: ItemFeedListAdBinding) :
        RecyclerView.ViewHolder(binding.root)

    class HouseAdFeedViewHolder(val binding: ItemFeedListHouseAdBinding) :
        RecyclerView.ViewHolder(binding.root)

    class RiverFeedViewHolder(val binding: ItemFeedListRiverBinding) :
        RecyclerView.ViewHolder(binding.root)

    class BigTopFeedViewHolder(val binding: ItemFeedListBigTopBinding) :
        RecyclerView.ViewHolder(binding.root)

    class SlideShowFeedViewHolder(val binding: ItemFeedListSlideshowBinding) :
        RecyclerView.ViewHolder(binding.root)
}