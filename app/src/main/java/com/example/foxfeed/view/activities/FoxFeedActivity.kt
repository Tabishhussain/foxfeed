package com.example.foxfeed.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.example.foxfeed.FoxFeedApplication
import com.example.foxfeed.R
import com.example.foxfeed.databinding.ActivityFoxFeedBinding
import com.example.foxfeed.view.adapters.FeedRecyclerAdapter
import com.example.foxfeed.viewModel.FoxFeedViewModel
import javax.inject.Inject

class FoxFeedActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFoxFeedBinding

    @Inject
    lateinit var foxFeedViewModel: FoxFeedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as FoxFeedApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityFoxFeedBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpView()
    }

    private fun setUpView() {
        val feedRecyclerAdapter = FeedRecyclerAdapter()
        binding.feedList.adapter = feedRecyclerAdapter
        binding.swipeToRefresh.setOnRefreshListener { foxFeedViewModel.loadFeeds() }

        //Livedata observer to listen for any change in feedLiveData to populate recycler view
        foxFeedViewModel.feedLiveData.observe(this) { feedList ->
            binding.swipeToRefresh.isRefreshing = false
            if (!feedList.isNullOrEmpty()) {
                feedRecyclerAdapter.setFeedList(feedList)
            }
        }

        foxFeedViewModel.error.observe(this) { errorString ->
            errorString?.let {
                Log.d("Error", errorString)
            }
        }
        foxFeedViewModel.loadFeeds()
    }
}