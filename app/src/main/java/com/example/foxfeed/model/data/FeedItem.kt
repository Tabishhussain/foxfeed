package com.example.foxfeed.model.data

data class FeedItem(val component: Component, val headline: String, val imageUrl: String?)

enum class Component {
    BIG_TOP,
    RIVER,
    AD,
    HOUSE_AD,
    SLIDE_SHOW
}

