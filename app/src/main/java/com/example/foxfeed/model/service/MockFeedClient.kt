package com.example.foxfeed.model.service

import android.util.Log
import com.example.foxfeed.model.data.Component
import com.example.foxfeed.model.data.FeedItem
import kotlin.random.Random

class MockFeedClient {

    fun getFeed() = buildFeed()

    private fun buildFeed(): List<FeedItem> {
        val items = mutableListOf<FeedItem>()
        Component.values().forEach {
            val max = Random.nextInt(1, 11)
            for (i in 0..max) {
                val item = FeedItem(it, "Headline #${i + 1} for $it", null)
                items.add(item)
            }
        }
        return items
    }
}