package com.example.foxfeed.model.repositories

import com.example.foxfeed.model.data.FeedItem
import com.example.foxfeed.model.service.MockFeedClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FeedRepository @Inject constructor(
    private val feedClient: MockFeedClient,
) {
    /*
    * Asynchronously loading feed idem from MockFeedClient
    * assuming it to be a background running task
    */
    suspend fun getFeed(
        feedLoadingListener: FeedLoadingListener
    ) = withContext(Dispatchers.IO) {
        Result.runCatching {
            feedClient.getFeed()
        }.onSuccess { feeds ->
            feedLoadingListener.onFinishLoading(feeds)
        }.onFailure { throwable ->
            feedLoadingListener.onFailure(throwable.message)
        }
    }

    /*
    * FeedLoadingListener provide functions to pass data from
    * FeedRepository to FoxFeedViewModel after fetching from Mock Client
    */
    interface FeedLoadingListener {

        //This callback provides list of feeds after a success loading of feeds
        fun onFinishLoading(feeds: List<FeedItem>)

        //This callback gets called in case of a failure while loading feeds
        fun onFailure(message: String?)
    }
}