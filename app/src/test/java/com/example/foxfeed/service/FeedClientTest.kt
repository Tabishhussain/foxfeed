package com.example.foxfeed.service

import com.example.foxfeed.model.data.Component
import com.example.foxfeed.model.service.MockFeedClient
import org.junit.Test

class FeedClientTest {

    @Test
    fun `test feed creation logic`() {
        val listOfFeed = MockFeedClient().getFeed()
        assert(listOfFeed.isNotEmpty())
        assert(listOfFeed.any { it.component == Component.BIG_TOP })
        assert(listOfFeed.any { it.component == Component.AD })
        assert(listOfFeed.any { it.component == Component.HOUSE_AD })
        assert(listOfFeed.any { it.component == Component.RIVER })
        assert(listOfFeed.any { it.component == Component.SLIDE_SHOW })
    }
}