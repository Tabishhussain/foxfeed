package com.example.foxfeed.repository

import com.example.foxfeed.model.data.Component
import com.example.foxfeed.model.data.FeedItem
import com.example.foxfeed.model.repositories.FeedRepository
import com.example.foxfeed.model.service.MockFeedClient
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import java.lang.IllegalArgumentException

class FeedRepositoryTest {

    @MockK
    lateinit var feedClient: MockFeedClient

    private lateinit var repository: FeedRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        repository = FeedRepository(feedClient)
    }

    @Test
    fun `when feed client return valid list of Feed OnFinishLoading called`() {
        every { feedClient.getFeed() } returns mockListOfFeeds()
        runBlocking {
            repository.getFeed(object : FeedRepository.FeedLoadingListener {
                override fun onFinishLoading(feeds: List<FeedItem>) {
                    assertEquals(5, feeds.size)
                }

                override fun onFailure(message: String?) {
                    fail("Should not called OnFailure")
                }

            })
        }
    }

    @Test
    fun `when feed client return failure OnFailure called`() {
        every { feedClient.getFeed() } throws IllegalArgumentException("ErrorLoading")
        runBlocking {
            repository.getFeed(object : FeedRepository.FeedLoadingListener {
                override fun onFinishLoading(feeds: List<FeedItem>) {
                    fail("Should not called OnFinishLoading")
                }

                override fun onFailure(message: String?) {
                    assertEquals("ErrorLoading", message)
                }
            })
        }
    }

    private fun mockListOfFeeds() = listOf<FeedItem>(
        FeedItem(Component.BIG_TOP, "Headline for Big Top", null),
        FeedItem(Component.AD, "Headline for Ad", null),
        FeedItem(Component.HOUSE_AD, "Headline for House Ad", null),
        FeedItem(Component.RIVER, "Headline for River", null),
        FeedItem(Component.SLIDE_SHOW, "Headline for slide show", null),
    )
}